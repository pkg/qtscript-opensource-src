Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2007, Free Software Foundation, Inc. <http://fsf.org/>
 2000-2002, 2007, 2008, Free Software Foundation, Inc.
 1989, 1991, Free Software Foundation, Inc.
License: GFDL-1.3 or GPL-2 or GPL-3 or LGPL-3

Files: .QT-ENTERPRISE-LICENSE-AGREEMENT
Copyright: Parties wish to enable that their respective Affiliates also can sell
License: LGPL-2.1+

Files: LICENSE.GPL3-EXCEPT
Copyright: 2007, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3 with Qt-GPL-1 exception

Files: debian/*
Copyright: 2007-2012 Fathi Boudra <fabo@debian.org>
            2007-2015 Sune Vuorela <debian@pusling.com>
            2008-2012 Modestas Vainius <modax@debian.org>
            2007-2009 Ana Beatriz Guerrero Lopez <ana@debian.org>
            2005-2007 Brian Nelson <pyro@debian.org>
            2012 Zoltán Balogh <zoltan.balogh@canonical.com>
            2012-2013 Timo Jyrinki <timo.jyrinki@canonical.com>
            2013 Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>
            2014-2022 Dmitry Shachnev <mitya57@debian.org>
License: LGPL-2.1

Files: examples/*
Copyright: 2017, The Qt Company Ltd.
License: BSD-3-clause

Files: examples/script/context2d/doc/*
Copyright: 2017, The Qt Company Ltd.
License: GFDL-1.3

Files: examples/script/defaultprototypes/doc/*
Copyright: 2017, The Qt Company Ltd.
License: GFDL-1.3

Files: examples/script/helloscript/doc/*
Copyright: 2017, The Qt Company Ltd.
License: GFDL-1.3

Files: src/*
Copyright: 2015, 2018, The Qt Company Ltd.
License: GPL-2 and/or LGPL-3

Files: src/3rdparty/*
Copyright: 2015, The Qt Company Ltd
 2009, University of Szeged
 2009, The Android Open Source Project
 2009, Patrick Gansterer (paroga@paroga.com)
 2009, Kevin Ollivier
 2009, Jian Li <jianli@chromium.org>
 2009, Ian C. Bullard
 2009, Company 100, Inc.
 2008, Torch Mobile Inc. (http://www.torchmobile.com/)
 2008, Kelvin W Sherlock (ksherlock@gmail.com)
 2008, Jürg Billeter <j@bitron.ch>
 2008, Dominik Röttsches <dominik.roettsches@access-company.com>
 2008, David Levin <levin@chromium.org>
 2008, Cameron Zwarich <cwzwarich@uwaterloo.ca>
 2008, Alp Toker <alp@atoker.com>
 2008, 2009, Torch Mobile Inc.
 2008, 2009, Paul Pedriana <ppedriana@ea.com>.
 2007-2009, Torch Mobile, Inc.
 2007, Staikos Computing Services Inc.
 2007, Justin Haygood (jhaygood@reaktix.com)
 2007, Eric Seidel <eric@webkit.org>
 2007, Cameron Zwarich (cwzwarich@uwaterloo.ca)
 2006-2008, the V8 project authors.
 2006, Samuel Weinig <sam.weinig@gmail.com>
 2006, George Staikos <staikos@kde.org>
 2006, Bjoern Graf (bjoern.graf@gmail.com)
 2006, Alexey Proskuryakov <ap@nypop.com>
 2006, 2007, Maks Orlovich
 2005-3009, Google Inc.
 2001, 2013, Peter Kelly (pmk@post.com)
 1999-2004, Harri Porten (porten@kde.org)
 1999, 2005, Pthreads-win32 contributors
 1998, John E. Bossom
 1997-2006, University of Cambridge
 1997-2002, Makoto Matsumoto and Takuji Nishimura
 1991, 2000, 2001, Lucent Technologies.
 1984, 1989-1991, 2000-2006, Free Software Foundation, Inc.
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/*
Copyright: 1991, Free Software Foundation, Inc.
License: LGPL-2

Files: src/3rdparty/javascriptcore/JavaScriptCore/API/*
Copyright: 2003, 2006-2009, Apple Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/API/APICast.h
 src/3rdparty/javascriptcore/JavaScriptCore/API/JSBase.h
 src/3rdparty/javascriptcore/JavaScriptCore/API/JSBasePrivate.h
 src/3rdparty/javascriptcore/JavaScriptCore/API/JSClassRef.h
 src/3rdparty/javascriptcore/JavaScriptCore/API/JSContextRef.h
 src/3rdparty/javascriptcore/JavaScriptCore/API/JSContextRefPrivate.h
 src/3rdparty/javascriptcore/JavaScriptCore/API/JSStringRef.h
 src/3rdparty/javascriptcore/JavaScriptCore/API/JSStringRefCF.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/API/JSStringRefCF.h
 src/3rdparty/javascriptcore/JavaScriptCore/API/JSValueRef.h
Copyright: 2006-2009, Apple Computer, Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/API/JSCallbackObject.cpp
Copyright: 2007, Eric Seidel <eric@webkit.org>
 2006, Apple Computer, Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/API/JSCallbackObject.h
 src/3rdparty/javascriptcore/JavaScriptCore/API/JSCallbackObjectFunctions.h
Copyright: 2007, Eric Seidel <eric@webkit.org>
 2006-2008, Apple Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/API/JSObjectRef.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/API/JSObjectRef.h
Copyright: 2008, Kelvin W Sherlock (ksherlock@gmail.com)
 2006-2008, Apple Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/API/JSRetainPtr.h
 src/3rdparty/javascriptcore/JavaScriptCore/API/JSStringRefBSTR.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/API/JSStringRefBSTR.h
Copyright: 2005-2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/API/JavaScript.h
Copyright: 2008, Alp Toker <alp@atoker.com>
 2006, Apple Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/DerivedSources.make
Copyright: 2005-2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/Info.plist
Copyright: 2003-2010, Apple Inc.; Copyright 1999-2001, Harri Porten &lt;porten@kde.org&gt;; Copyright 2001, Peter Kelly &lt;pmk@post.com&gt;; Copyright 1997-2005, University of Cambridge; Copyright 1991, 2000, 2001,Lucent Technologies.</string>
License: UNKNOWN

Files: src/3rdparty/javascriptcore/JavaScriptCore/assembler/*
Copyright: 2003, 2006-2009, Apple Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/assembler/ARMAssembler.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/assembler/ARMAssembler.h
 src/3rdparty/javascriptcore/JavaScriptCore/assembler/AssemblerBufferWithConstantPool.h
 src/3rdparty/javascriptcore/JavaScriptCore/assembler/MacroAssemblerARM.cpp
Copyright: 2009, University of Szeged
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/assembler/MacroAssemblerARM.h
Copyright: 2009, University of Szeged
 2008, Apple Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/bytecode/*
Copyright: 2008, Cameron Zwarich <cwzwarich@uwaterloo.ca>
 2008, 2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/bytecode/EvalCodeCache.h
 src/3rdparty/javascriptcore/JavaScriptCore/bytecode/Instruction.h
 src/3rdparty/javascriptcore/JavaScriptCore/bytecode/SamplingTool.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/bytecode/SamplingTool.h
Copyright: 2005-2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/bytecode/StructureStubInfo.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/bytecode/StructureStubInfo.h
Copyright: 2003, 2006-2009, Apple Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/bytecompiler/*
Copyright: 2005-2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/bytecompiler/BytecodeGenerator.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/bytecompiler/BytecodeGenerator.h
Copyright: 2008, Cameron Zwarich <cwzwarich@uwaterloo.ca>
 2008, 2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/bytecompiler/NodesCodegen.cpp
Copyright: 2007, Maks Orlovich
 2007, Eric Seidel <eric@webkit.org>
 2007, Cameron Zwarich (cwzwarich@uwaterloo.ca)
 2003-2009, Apple Inc.
 2001, Peter Kelly (pmk@post.com)
 1999-2002, Harri Porten (porten@kde.org)
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/config.h
Copyright: 2006-2008, Apple Inc.
 2006, Samuel Weinig <sam.weinig@gmail.com>
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/create_hash_table
Copyright: 2007-2009, Apple Inc.
 2004, Nikolas Zimmermann <wildfox@kde.org>
 2000-2002, Harri Porten <porten@kde.org> and
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/debugger/*
Copyright: 2003, 2006-2009, Apple Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/debugger/Debugger.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/debugger/Debugger.h
Copyright: 2003-2009, Apple Inc.
 2001, Peter Kelly (pmk@post.com)
 1999-2002, Harri Porten (porten@kde.org)
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/debugger/DebuggerCallFrame.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/debugger/DebuggerCallFrame.h
Copyright: 2005-2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/generated/*
Copyright: 1984, 1989, 1990, 2000-2006, Free Software Foundation, Inc.
License: (GPL-3+ and/or LGPL-2+) with Bison-2.2 exception

Files: src/3rdparty/javascriptcore/JavaScriptCore/generated/Grammar.h
Copyright: 1984, 1989, 1990, 2000-2006, Free Software Foundation, Inc.
License: GPL-3+ with Bison-2.2 exception

Files: src/3rdparty/javascriptcore/JavaScriptCore/interpreter/*
Copyright: 2005-2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/interpreter/CachedCall.h
 src/3rdparty/javascriptcore/JavaScriptCore/interpreter/CallFrame.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/interpreter/CallFrameClosure.h
Copyright: 2003, 2006-2009, Apple Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/interpreter/CallFrame.h
Copyright: 2003-2009, Apple Inc.
 2001, Peter Kelly (pmk@post.com)
 1999-2002, Harri Porten (porten@kde.org)
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/interpreter/Interpreter.cpp
Copyright: 2008, Cameron Zwarich <cwzwarich@uwaterloo.ca>
 2008, 2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/jit/*
Copyright: 2003, 2006-2009, Apple Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/jit/ExecutableAllocatorSymbian.cpp
Copyright: 2015, The Qt Company Ltd
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/jit/JITStubs.cpp
Copyright: 2008, Cameron Zwarich <cwzwarich@uwaterloo.ca>
 2008, 2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/jit/JITStubs.h
Copyright: 2005-2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/jsc.cpp
Copyright: 2006, Bjoern Graf (bjoern.graf@gmail.com)
 2004-2008, Apple Inc.
 1999, 2000, Harri Porten (porten@kde.org)
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/os-win32/*
Copyright: 2005, 2006, Apple Computer, Inc.
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/os-win32/WinMain.cpp
Copyright: 2009, Patrick Gansterer (paroga@paroga.com)
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/parser/*
Copyright: 2003, 2006-2009, Apple Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/parser/Grammar.y
Copyright: 2007, Eric Seidel <eric@webkit.org>
 2006-2009, Apple Inc.
 1999, 2000, Harri Porten (porten@kde.org)
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/parser/Lexer.cpp
Copyright: 2007, Cameron Zwarich (cwzwarich@uwaterloo.ca)
 2006-2009, Apple Inc.
 1999, 2000, Harri Porten (porten@kde.org)
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/parser/Lexer.h
Copyright: 2002-2009, Apple Inc.
 1999-2001, 2003, Harri Porten (porten@kde.org)
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/parser/NodeConstructors.h
 src/3rdparty/javascriptcore/JavaScriptCore/parser/NodeInfo.h
Copyright: 2003-2010, Apple Inc.
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/parser/Nodes.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/parser/Nodes.h
Copyright: 2007, Maks Orlovich
 2007, Eric Seidel <eric@webkit.org>
 2007, Cameron Zwarich (cwzwarich@uwaterloo.ca)
 2003-2009, Apple Inc.
 2001, Peter Kelly (pmk@post.com)
 1999-2002, Harri Porten (porten@kde.org)
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/parser/Parser.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/parser/Parser.h
Copyright: 2003-2009, Apple Inc.
 2001, Peter Kelly (pmk@post.com)
 1999-2002, Harri Porten (porten@kde.org)
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/parser/SourceCode.h
 src/3rdparty/javascriptcore/JavaScriptCore/parser/SourceProvider.h
Copyright: 2005-2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/pcre/*
Copyright: 1997-2005, University of Cambridge.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/pcre/AUTHORS
Copyright: 2005-2007, Apple Inc.
 1997-2005, University of Cambridge.
License: UNKNOWN

Files: src/3rdparty/javascriptcore/JavaScriptCore/pcre/dftables
 src/3rdparty/javascriptcore/JavaScriptCore/pcre/pcre.h
 src/3rdparty/javascriptcore/JavaScriptCore/pcre/pcre_internal.h
 src/3rdparty/javascriptcore/JavaScriptCore/pcre/pcre_tables.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/pcre/pcre_ucp_searchfuncs.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/pcre/pcre_xclass.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/pcre/ucpinternal.h
Copyright: 2002, 2004, 2006-2009, Apple Inc.
 1997-2006, University of Cambridge
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/pcre/pcre_compile.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/pcre/pcre_exec.cpp
Copyright: 2007, Eric Seidel <eric@webkit.org>
 2002, 2004, 2006-2009, Apple Inc.
 1997-2006, University of Cambridge
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/profiler/*
Copyright: 2003, 2006-2009, Apple Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/profiler/ProfileNode.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/profiler/ProfileNode.h
 src/3rdparty/javascriptcore/JavaScriptCore/profiler/Profiler.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/profiler/Profiler.h
Copyright: 2005-2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/*
Copyright: 2002-2009, Apple Inc.
 1999-2001, 2003, Harri Porten (porten@kde.org)
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/ArgList.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/CommonIdentifiers.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/CommonIdentifiers.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/Identifier.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/Identifier.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSImmediate.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSLock.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSLock.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSType.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/Lookup.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/PropertyMapHashTable.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/PropertyNameArray.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/PropertyNameArray.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/PropertySlot.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/PropertySlot.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/Protect.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/RegExpMatchesArray.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/ScopeChain.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/ScopeChain.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/ScopeChainMark.h
Copyright: 2003-2010, Apple Inc.
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/Arguments.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/GlobalEvalFunction.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSFunction.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSGlobalObjectFunctions.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/PrototypeFunction.cpp
Copyright: 2007, Maks Orlovich
 2007, Cameron Zwarich (cwzwarich@uwaterloo.ca)
 2003-2009, Apple Inc.
 2001, Peter Kelly (pmk@post.com)
 1999-2002, Harri Porten (porten@kde.org)
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/Arguments.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/GlobalEvalFunction.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/InternalFunction.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSFunction.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSGlobalObjectFunctions.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/PrototypeFunction.h
Copyright: 2007, Maks Orlovich
 2007, Cameron Zwarich (cwzwarich@uwaterloo.ca)
 2003, 2006-2009, Apple Inc.
 1999, 2000, Harri Porten (porten@kde.org)
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/ArrayConstructor.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/ArrayPrototype.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSArray.cpp
Copyright: 2006, Alexey Proskuryakov (ap@nypop.com)
 2003, Peter Kelly (pmk@post.com)
 2003, 2007-2009, Apple Inc.
 1999, 2000, Harri Porten (porten@kde.org)
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/BatchedTransitionOptimizer.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/CallData.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/CollectorHeapIterator.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/ConstructData.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/DateInstanceCache.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/Executable.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/Executable.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSByteArray.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSByteArray.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSONObject.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSONObject.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSStaticScopeObject.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSStaticScopeObject.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSTypeInfo.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSZombie.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSZombie.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/LiteralParser.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/LiteralParser.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/MarkStack.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/MarkStack.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/MarkStackPosix.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/MarkStackWin.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/NativeFunctionWrapper.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/NumericStrings.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/PropertyDescriptor.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/PropertyDescriptor.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/PutPropertySlot.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/SmallStrings.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/SmallStrings.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/StringBuilder.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/Structure.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/Structure.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/StructureChain.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/StructureChain.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/StructureTransitionTable.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/Tracing.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/UStringImpl.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/UStringImpl.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/WeakGCMap.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/WeakGCPtr.h
Copyright: 2003, 2006-2009, Apple Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/CallData.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/ConstructData.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/ExceptionHelpers.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/ExceptionHelpers.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/InitializeThreading.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/InitializeThreading.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSActivation.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSActivation.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSGlobalData.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSGlobalData.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSNotAnObject.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSNotAnObject.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSPropertyNameIterator.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSPropertyNameIterator.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSVariableObject.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSVariableObject.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/SymbolTable.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/TimeoutChecker.h
Copyright: 2005-2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/ClassInfo.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/Collector.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/Completion.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/Completion.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/Error.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/GetterSetter.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/GetterSetter.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/InternalFunction.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSAPIValueWrapper.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSAPIValueWrapper.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSCell.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSCell.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSNumberCell.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSNumberCell.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSObject.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSString.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSString.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSValue.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSValue.h
Copyright: 2003-2009, Apple Inc.
 2001, Peter Kelly (pmk@post.com)
 1999-2002, Harri Porten (porten@kde.org)
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/Collector.cpp
Copyright: 2007, Eric Seidel <eric@webkit.org>
 2003-2009, Apple Inc.
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/DateConversion.cpp
Copyright: 2009, Google Inc.
 2006, 2007, Apple Inc.
 1999, 2000, Harri Porten (porten@kde.org)
License: LGPL-2.1+ and/or MPL-1.1

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/DateConversion.h
Copyright: 2009, Google Inc.
 2006, 2007, Apple Inc.
 1999, 2000, Harri Porten (porten@kde.org)
License: GPL and/or MPL-1.1

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/DatePrototype.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/RegExp.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/RegExp.h
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/RegExpConstructor.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/StringPrototype.cpp
Copyright: 2008, 2009, Torch Mobile, Inc.
 2003-2009, Apple Inc.
 1999-2001, 2004, Harri Porten (porten@kde.org)
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/Error.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSObject.cpp
Copyright: 2007, Eric Seidel (eric@webkit.org)
 2003-2006, 2008, 2009, Apple Inc.
 2001, Peter Kelly (pmk@post.com)
 1999-2001, Harri Porten (porten@kde.org)
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSGlobalObject.cpp
Copyright: 2008, Cameron Zwarich (cwzwarich@uwaterloo.ca)
 2007-2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSGlobalObject.h
Copyright: 2007-2009, Apple Inc.
 2007, Eric Seidel <eric@webkit.org>
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSImmediate.h
Copyright: 2006, Alexey Proskuryakov (ap@webkit.org)
 2003-2009, Apple Inc.
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSWrapperObject.cpp
Copyright: 2006, Maks Orlovich
 2006, 2009, Apple, Inc.
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/JSWrapperObject.h
Copyright: 2006-2009, Apple Inc.
 2006, Maks Orlovich
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/MarkStackNone.cpp
Copyright: 2009, Company 100, Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/MarkStackSymbian.cpp
Copyright: 2015, The Qt Company Ltd
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/TimeoutChecker.cpp
Copyright: 2008, Cameron Zwarich <cwzwarich@uwaterloo.ca>
 2008, 2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/UString.cpp
Copyright: 2009, Google Inc.
 2007, Cameron Zwarich (cwzwarich@uwaterloo.ca)
 2004-2009, Apple Inc.
 1999, 2000, Harri Porten (porten@kde.org)
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/UString.h
Copyright: 2009, Google Inc.
 2004-2009, Apple Inc.
 1999, 2000, Harri Porten (porten@kde.org)
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/runtime/WeakRandom.h
Copyright: 2009, Apple Inc.
License: BSD-2-clause and/or Expat

Files: src/3rdparty/javascriptcore/JavaScriptCore/wrec/*
Copyright: 2003, 2006-2009, Apple Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wscript
Copyright: 2009, Kevin Ollivier
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/*
Copyright: 2003-2010, Apple Inc.
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/ASCIICType.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/AVLTree.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/DisallowCType.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/Locker.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/MainThread.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/MallocZoneSupport.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/SegmentedVector.h
Copyright: 2005-2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/Assertions.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/OwnPtrCommon.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/OwnPtrWin.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/Platform.h
Copyright: 2007-2009, Torch Mobile, Inc.
 2003, 2006-2009, Apple Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/Assertions.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/ByteArray.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/ByteArray.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/HashIterators.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/MathExtras.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/NotFound.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/PassOwnPtr.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/PossiblyNull.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/StdLibExtras.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/StringExtras.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/Threading.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/VMTags.h
Copyright: 2003, 2006-2009, Apple Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/CrossThreadRefCounted.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/PtrAndFlags.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/TCPackedCache.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/TCPageMap.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/TCSpinLock.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/TCSystemAlloc.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/TCSystemAlloc.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/ThreadIdentifierDataPthreads.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/ThreadIdentifierDataPthreads.h
Copyright: 2005-2009, Google Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/CurrentTime.cpp
Copyright: 2008, Google Inc.
 2007-2009, Torch Mobile, Inc.
 2006, Apple Computer, Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/CurrentTime.h
Copyright: 2008, Google Inc.
 2006, Apple Computer, Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/DateMath.cpp
Copyright: 2009, Google Inc.
 2007-2009, Torch Mobile, Inc.
 2006, 2007, Apple Inc.
 1999, 2000, Harri Porten (porten@kde.org)
License: BSD-3-clause and/or LGPL-2.1+ and/or MPL-1.1

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/DateMath.h
Copyright: 2009, Google Inc.
 2006, 2007, Apple Inc.
 1999, 2000, Harri Porten (porten@kde.org)
License: GPL and/or MPL-1.1

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/Deque.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/MessageQueue.h
Copyright: 2009, Google Inc.
 2007, 2008, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/FastAllocBase.h
Copyright: 2008, 2009, Paul Pedriana <ppedriana@ea.com>.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/FastMalloc.cpp
Copyright: 2005-2009, Apple Inc.
 2005, 2007, Google Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/GetPtr.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/Noncopyable.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/OwnArrayPtr.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/UnusedParam.h
Copyright: 2005, 2006, Apple Computer, Inc.
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/HashTable.h
Copyright: 2008, David Levin <levin@chromium.org>
 2005-2008, Apple Inc.
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/MainThread.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/ThreadingNone.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/ThreadingPthreads.cpp
Copyright: 2007, Justin Haygood (jhaygood@reaktix.com)
 2007-2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/OwnFastMallocPtr.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/TypeTraits.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/TypeTraits.h
Copyright: 2009, 2010, Google Inc.
 2006-2008, Apple Inc.
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/RandomNumber.cpp
Copyright: 2008, 2009, Torch Mobile Inc.
 2006-2008, Apple Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/RandomNumber.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/RandomNumberSeed.h
Copyright: 2008, Torch Mobile Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/StringExtras.cpp
Copyright: 2009, Company 100, Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/ThreadSpecific.h
Copyright: 2009, Jian Li <jianli@chromium.org>
 2008, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/ThreadSpecificWin.cpp
Copyright: 2009, Jian Li <jianli@chromium.org>
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/Threading.h
Copyright: 2007, Justin Haygood (jhaygood@reaktix.com)
 2007, 2008, Apple Inc.
License: BSD-3-clause and/or BSL-1.0

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/ThreadingWin.cpp
Copyright: 2009, Torch Mobile, Inc.
 2009, Google Inc.
 2007, 2008, Apple Inc.
License: BSD-3-clause and/or LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/android/*
Copyright: 2009, The Android Open Source Project
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/dtoa.cpp
Copyright: 2002, 2005-2008, Apple Inc.
 1991, 2000, 2001, Lucent Technologies.
License: UNKNOWN

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/qt/*
Copyright: 2015, The Qt Company Ltd
 2008, Apple Inc.
 2007, Staikos Computing Services Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/qt/ThreadingQt.cpp
Copyright: 2007, Justin Haygood (jhaygood@reaktix.com)
 2007-2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/symbian/*
Copyright: 2015, The Qt Company Ltd
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/unicode/*
Copyright: 2006-2009, Apple Inc.
 2006, George Staikos <staikos@kde.org>
 2006, Alexey Proskuryakov <ap@nypop.com>
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/unicode/Collator.h
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/unicode/CollatorDefault.cpp
Copyright: 2005-2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/unicode/UTF8.cpp
 src/3rdparty/javascriptcore/JavaScriptCore/wtf/unicode/UTF8.h
Copyright: 2003, 2006-2009, Apple Inc.
License: BSD-2-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/unicode/Unicode.h
Copyright: 2007-2009, Torch Mobile, Inc.
 2006, George Staikos <staikos@kde.org>
 2006, 2008, 2009, Apple Inc.
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/unicode/glib/*
Copyright: 2008, JÃ¼rg Billeter <j@bitron.ch>
 2008, Dominik RÃ¶ttsches <dominik.roettsches@access-company.com>
 2007, Apple Computer, Inc.
 2006, George Staikos <staikos@kde.org>
 2006, Alexey Proskuryakov <ap@nypop.com>
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/unicode/glib/UnicodeGLib.cpp
Copyright: 2008, JÃ¼rg Billeter <j@bitron.ch>
 2008, Dominik RÃ¶ttsches <dominik.roettsches@access-company.com>
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/unicode/icu/CollatorICU.cpp
Copyright: 2005-2009, Apple Inc.
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/unicode/wince/*
Copyright: 2007-2009, Torch Mobile, Inc.
 2006, George Staikos <staikos@kde.org>
 2006, Alexey Proskuryakov <ap@nypop.com>
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/unicode/wince/UnicodeWince.h
Copyright: 2007-2009, Torch Mobile, Inc.
 2007, Apple Computer, Inc.
 2006, George Staikos <staikos@kde.org>
 2006, Alexey Proskuryakov <ap@nypop.com>
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/wince/*
Copyright: 2008, 2009, Torch Mobile Inc.
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/wince/FastMallocWince.h
Copyright: 2007-2009, Torch Mobile, Inc.
 2005-2008, Apple Inc.
License: LGPL-2+

Files: src/3rdparty/javascriptcore/JavaScriptCore/wtf/wince/mt19937ar.c
Copyright: 1997-2002, Makoto Matsumoto and Takuji Nishimura
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/yarr/*
Copyright: 2003, 2006-2009, Apple Inc.
License: BSD-2-clause

Files: src/script/api/qtscriptglobal.h
Copyright: 2018, Intel Corporation.
License: GPL-2 and/or LGPL-3

Files: src/script/doc/*
Copyright: 2017, The Qt Company Ltd.
License: BSD-3-clause

Files: src/script/doc/snippets/code/doc_src_examples_hellotr.qdoc
Copyright: 2013, Digia Plc and/or its subsidiary(-ies).
License: BSD-3-clause

Files: src/script/doc/src/*
Copyright: 2017, The Qt Company Ltd.
License: GFDL-1.3

Files: src/script/doc/src/qtscript-module.qdoc
Copyright: 2017, The Qt Company Ltd.
License: GFDL-1.3 and/or LGPL-2+

Files: src/scripttools/doc/*
Copyright: 2017, The Qt Company Ltd.
License: BSD-3-clause

Files: src/scripttools/doc/src/*
Copyright: 2017, The Qt Company Ltd.
License: GFDL-1.3

Files: tests/*
Copyright: 2018, The Qt Company Ltd.
License: GPL-3

Files: tests/auto/qscriptengine/translations/*
Copyright: no-info-found
License: UNKNOWN

Files: tests/auto/qscriptjstestsuite/tests/*
Copyright: 1998, 2001-2003, 2005-2008, the Initial Developer.
License: GPL-2+ and/or GPL-2+ or LGPL-2.1+ and/or MPL-1.1

Files: tests/auto/qscriptjstestsuite/tests/ecma_3/extensions/regress-188206-02.js
Copyright: 2003, the Initial Developer.
License: GPL-2+ and/or GPL-2+ or LGPL-2.1+ and/or MPL-1.1 and/or Perl

Files: tests/auto/qscriptengine/translations/*
Copyright: 2018 The Qt Company Ltd.
License: GPL-3 with Qt-1.0 exception

Files: src/3rdparty/javascriptcore/JavaScriptCore/pcre/AUTHORS
Copyright: 1997-2005 University of Cambridge
License: BSD-3-clause

Files: src/3rdparty/javascriptcore/JavaScriptCore/Info.plist src/3rdparty/javascriptcore/JavaScriptCore/wtf/dtoa.cpp
Copyright: 1999-2003 Harri Porten <porten@kde.org>
 2001 Peter Kelly <pmk@post.com>
 2003-2009 Apple Inc. All rights reserved
 2007 Eric Seidel <eric@webkit.org>
 2006-2007 Maks Orlovich
 2006 Alexey Proskuryakov <ap@nypop.com>
 2008 Cameron Zwaric <cwzwarich@uwaterloo.ca>
 2006 Bjoern Graf <bjoern.graf@gmail.com>
License: LGPL-2+

Files: tests/auto/qscriptqwidgets/*
Copyright: 2018, Klarälvdalens Datakonsult AB, a KDAB Group company, info@kdab.com, author Stephen Kelly <stephen.kelly@kdab.com>
License: GPL-3

Files: tests/auto/qscriptv8testsuite/tests/*
Copyright: 2005-2009, Google Inc.
License: BSD-3-clause

